﻿namespace AdventOfCode2022.Day3;

public static class Day3
{
    public static void PartOne()
    {
        var input = AocHelper.GetInputAsStringList("3");
        var result = 0;
        foreach (var line in input)
        {
            foreach (var c in line.Substring(0, line.Length / 2).Where(c => line.Substring(line.Length / 2).Contains(c)))
            {
                result += GetPriority(c);
                break;
            }
        }
        Console.WriteLine("SumOfPriorities: " + result);
    }

    public static void PartTwo()
    {
        var input = AocHelper.GetInputAsString("3").Split("\r\n").Chunk(3).ToList();
        var result = 0;
        
        foreach (var list in input)
        {
            foreach (var c in list[0])
            {
                if (!list[1].Contains(c) || !list[2].Contains(c)) continue;
                result += GetPriority(c);
                break;
            }
        }
        Console.WriteLine("SumOfPriorities: " + result);
    }

    private static int GetPriority(char c)
    {
        if (c >= 'a' && c <= 'z')
            return c - 'a' + 1;
        return c - 'A' + 1 + 26;
    }
}