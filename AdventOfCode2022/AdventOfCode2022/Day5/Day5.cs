﻿namespace AdventOfCode2022.Day5;

public static class Day5
{
    private static List<string[]> _moveInput = AocHelper.GetInputAsString("5").Split("\r\n").Select(line => line.Split(' ')).ToList();
    private static List<List<char>> _cratesList = new()
    {
        new List<char> {'Z', 'J', 'G'},
        new List<char> {'Q', 'L', 'R', 'P', 'W', 'F', 'V', 'C'},
        new List<char> {'F', 'P', 'M', 'C', 'L', 'G', 'R'},
        new List<char> {'L', 'F', 'B', 'W', 'P', 'H', 'M'},
        new List<char> {'G', 'C', 'F', 'S', 'V', 'Q'},
        new List<char> {'W', 'H', 'J', 'Z', 'M', 'Q', 'T', 'L'},
        new List<char> {'H', 'F', 'S', 'B', 'V'},
        new List<char> {'F', 'J', 'Z', 'S'},
        new List<char> {'M', 'C', 'D', 'P', 'F', 'H', 'B', 'T'}
    };

    public static void PartOne()
    {
        var topCrates = "";
        foreach (var move in _moveInput)
        {
            var quantity = int.Parse(move[1]);
            var stackOne = int.Parse(move[3])-1;
            var stackTwo = int.Parse(move[5])-1;
            
            for (var i = 0; i < quantity; i++)
            {
                var movingCrate = _cratesList[stackOne].Last();
                _cratesList[stackTwo].Add(movingCrate);
                _cratesList[stackOne].RemoveAt(_cratesList[stackOne].Count-1);
            }
        }

        foreach (var list in _cratesList)
        {
            topCrates += list.Last();
        }
        Console.WriteLine("TopCrates: " + topCrates);
    }

    public static void PartTwo()
    {
        var topCrates = "";
        foreach (var move in _moveInput)
        {
            var quantity = int.Parse(move[1]);
            var stackOne = int.Parse(move[3])-1;
            var stackTwo = int.Parse(move[5])-1;
            var movingCrates = Enumerable.Reverse(_cratesList[stackOne]).Take(quantity).Reverse().ToList();

            foreach (var crate in movingCrates)
            {
                _cratesList[stackTwo].Add(crate);
                _cratesList[stackOne].RemoveAt(_cratesList[stackOne].Count-1);
            }
        }

        foreach (var list in _cratesList)
        {
            topCrates += list.Last();
        }
        Console.WriteLine("TopCrates: " + topCrates);
    }
}