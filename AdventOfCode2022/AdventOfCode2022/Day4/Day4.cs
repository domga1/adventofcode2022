﻿namespace AdventOfCode2022.Day4;

public static class Day4
{
   private static readonly char[] Separators = { '-', ',' };
   private static readonly List<string[]> Input = AocHelper.GetInputAsString("4").Split("\r\n").Select(line => line.Split(Separators)).ToList();
   public static void PartOne()
   {
      var fullyContainingPairs = 0;

      foreach (var parsedPairs in Input.Select(pairs => pairs.Select(int.Parse).ToList()))
      {
         if (parsedPairs[0] < parsedPairs[2])
         {
            if (parsedPairs[1] >= parsedPairs[3]) fullyContainingPairs++;
         }
         else if (parsedPairs[0] == parsedPairs[2]) fullyContainingPairs++;
         else if (parsedPairs[1] <= parsedPairs[3]) fullyContainingPairs++;
      }
      Console.WriteLine("FullyContainingPairs: " + fullyContainingPairs);
   }

   public static void PartTwo()
   {
      var overlappingPairs = 0;

      foreach (var parsedPairs in Input.Select(pairs => pairs.Select(int.Parse).ToList()))
      {
         if (parsedPairs[0] == parsedPairs[2] || parsedPairs[1] == parsedPairs[2]) overlappingPairs++;
         else if (parsedPairs[0] >= parsedPairs[2] && parsedPairs[0] <= parsedPairs[3]) overlappingPairs++;
         else if (parsedPairs[0] <= parsedPairs[2] && parsedPairs[1] >= parsedPairs[2]) overlappingPairs++;
      }
      Console.WriteLine("OverlappingPairs: " + overlappingPairs);
   }
}