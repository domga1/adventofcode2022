﻿namespace AdventOfCode2022;

public static class AocHelper
{
    public static string GetInputPath(string day)
    {
        return @"C:\Repos\adventofcode2022\AdventOfCode2022\AdventOfCode2022\Day"+ day + "\\input.txt";
    }

    public static string GetInputAsString(string day)
    {
        var path = GetInputPath(day);
        return File.ReadAllText(@"C:\Repos\adventofcode2022\AdventOfCode2022\AdventOfCode2022\Day"+ day + "\\input.txt");
    }

    public static List<string> GetInputAsStringList(string day)
    {
        return File.ReadLines(@"C:\Repos\adventofcode2022\AdventOfCode2022\AdventOfCode2022\Day"+ day + "\\input.txt").ToList();
    }
}