﻿namespace AdventOfCode2022.Day2;
public static class Day2
{
    public static void PartOne()
    {
        var scores = new Dictionary<string, int>()
        {
            {"A X", 4}, {"A Y", 8}, {"A Z", 3}, 
            {"B X", 1}, {"B Y", 5}, {"B Z", 9},
            {"C X", 7}, {"C Y", 2}, {"C Z", 6}
        };
        
        Console.WriteLine("PartOne: " + GetPoints(scores));
    }
    
    public static void PartTwo()
    {
        var scores = new Dictionary<string, int>()
        {
            {"A X", 3}, {"A Y", 4}, {"A Z", 8}, 
            {"B X", 1}, {"B Y", 5}, {"B Z", 9},
            {"C X", 2}, {"C Y", 6}, {"C Z", 7}
        };
       
        Console.WriteLine("PartTwo: " + GetPoints(scores));
    }

    private static int GetPoints(IReadOnlyDictionary<string, int> scores)
    {
        var input = AocHelper.GetInputAsString("2").Split("\r\n").ToList();
        return input.Where(play => scores.ContainsKey(play)).Sum(play => scores[play]);
    }
}