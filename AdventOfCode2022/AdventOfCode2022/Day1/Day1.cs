﻿namespace AdventOfCode2022.Day1;

public static class Day1 {
       
    public static void PartOneAndTwo()
    {
        var input = AocHelper.GetInputAsStringList("1");
        var caloriesList = new List<int>();
        var tempList = new List<int>();
         
        foreach (var line in input)
        {
            if (line == "")
            {
                var maxCalories = tempList.Sum();
                caloriesList.Add(maxCalories);
                tempList.Clear();
            }
            if (int.TryParse(line, out _)) tempList.Add(int.Parse(line));
        }

        caloriesList.Sort();
        caloriesList.Reverse();

        var highestCalories = caloriesList[0];
        var highestThreeCalories = caloriesList[0] + caloriesList[1] + caloriesList[2];
            
        Console.WriteLine("Highest Calories: " + highestCalories + " Top three calorie carrying elves: " + highestThreeCalories);
            
        //Solution from the internet :/
        // Console.WriteLine(AocHelper.GetInputAsString("1").Split("\r\n\r\n").ToList().Select(y => y.Split("\r\n").ToList().Select(int.Parse).Sum()).Max());
        // Console.WriteLine(AocHelper.GetInputAsString("1").Split("\r\n\r\n").ToList().Select(y => y.Split("\r\n").ToList().Select(int.Parse).Sum()).ToList().OrderByDescending(n => n).Take(3).Sum());
    }
}