﻿namespace AdventOfCode2022;

internal static class Start {
    static void Main() {
            
        //--- Day 1 ---
        // Day1.Day1.PartOneAndTwo();
            
        //--- Day 2 ---
        // Day2.Day2.PartOne();
        // Day2.Day2.PartTwo();
        
        //--- Day 3 ---
        // Day3.Day3.PartOne();
        // Day3.Day3.PartTwo();
     
        //--- Day 4 ---
        // Day4.Day4.PartOne();
        // Day4.Day4.PartTwo();
        
        //--- Day 5 ---
        // Day5.Day5.PartOne();
        Day5.Day5.PartTwo();
    }
}